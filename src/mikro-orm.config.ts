import { Logger } from '@nestjs/common';
import { SqlHighlighter } from '@mikro-orm/sql-highlighter';
import { defineConfig } from '@mikro-orm/mysql';
import { BaseEntity, User } from './entities';

const logger = new Logger('MikroORM');

export default defineConfig({
  entities: [BaseEntity, User],
  dbName: 'linkz-asia-testcase',
  port: 3307,
  highlighter: new SqlHighlighter(),
  debug: true,
  logger: logger.log.bind(logger),
});
